package app;

import static io.restassured.RestAssured.*;

import java.util.List;
import java.util.Map;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.http.Method;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;


public class App 
{
    public static void main( String[] args )
    {
    	Response response = RestAssured.request(Method.GET,"https://api.trello.com/1/actions/592f11060f95a3d3d46a987a");
        System.out.println( response.getBody().asString());
        System.out.println( response.jsonPath().getString("name.list"));
        System.out.println( response.statusCode());

    }
  
   
}
