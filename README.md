# Projeto Automação de teste Java/JUnit/API REST-Assured

- Java + JUnit + API REST-Assured.

# O setup de construção e execução:
- [x] Windows 10
- [x] Instalado:
	- Java JDK/JRE 1.8  
	- apache-maven-3.6.2
	- Eclipse Neon.3
	- Gitbash
	- Insomnia (Software para auxiliar com a request/response)

# O Projeto deve ser importado no Eclipse como "Existing Maven Projects"
<img src="import_project_eclipse.jpg">


# A execução no Eclipse pode ser feito diretamente no arquivo "TestTrelloAPI.java"
- [x] src/test/java/tests/TestTrelloAPI.java

# Segue a estrutura de pastas do projeto:
<img src="test_api_trello.jpg">