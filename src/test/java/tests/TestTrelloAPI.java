package tests;

import org.junit.Assert;
import org.junit.Test;
import static io.restassured.RestAssured.*;
import static support.Helper.*;

import java.util.List;
import java.util.Map;

import javax.swing.text.html.ListView;

import com.sun.corba.se.spi.ior.iiop.GIOPVersion;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import support.*;

public class TestTrelloAPI {

	@Test
	public void TestREST_API(){
		Response response = fazerGetRequest("https://api.trello.com/1/actions/592f11060f95a3d3d46a987a");
		Map<String, String> list = response.jsonPath().getMap("data.list");
		System.out.println("Name: " +list.get("name"));
		
		Assert.assertTrue("StatusCode deveria ser 200",response.statusCode() == 200);
        Assert.assertEquals(200, response.statusCode());
        System.out.println( "StatusCode: " +response.statusCode());
 	}
	
}
